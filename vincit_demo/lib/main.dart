import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vincit_demo/stream/GameConnection.dart';
import 'package:vincit_demo/providers/api.dart';
import 'package:vincit_demo/providers/storage.dart';
import 'package:vincit_demo/views/AdminView.dart';
import 'package:vincit_demo/views/GameWrapper.dart';
import 'package:vincit_demo/views/Login.dart';
import 'package:vincit_demo/widgets/dialogs/LoadingDialog.dart';
import 'package:vincit_demo/wrappers/AppState.dart';

import 'models/User.dart';

void main() => runApp(ClickerGame());

class ClickerGame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Vincit demo",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool connecting = false;
  bool _hasLoggedIn = false;
  bool _loading = true;
  Api _api;
  User _self;
  GameConnection _connection = GameConnection();

  @override
  void dispose() {
    //Terminate connection only on app exit
    _connection?.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    //Socket attempts to reconnect on disconnect
    _connection.onDisconnect = () {
      connecting = true;
      showLoadingDialog("Hang tight, we're reconnecting", context);
    };

    //Hide loading dialog
    _connection.onConnect = (_) {
      if (connecting) Navigator.of(context).pop();
      connecting = false;
    };

    isLoggedIn().then((bool logged) async {
      if (logged) {
        String token = await getToken();
        Api a = Api(token);

        _connection.connect(url, token);

        //Load self from server
        try {
          User self = await a.getSelf();
          setState(() {
            _loading = false;
            _api = a;
            _self = self;
            _hasLoggedIn = true;
          });
        } catch (e) {
          //Caused by a server error
          //Log out just to make sure
          print(e);
          logOut();
          setState(() {
            _loading = false;
            _hasLoggedIn = false;
          });
        }
      } else {
        setState(() {
          _loading = false;
          _hasLoggedIn = logged;
        });
      }
    });
  }

  void didLogin(String token) async {
    if (token != null && token.isNotEmpty) {
      if (!_connection.isConnected) {
        _connection.connect(url, token);
      } else {
        //We have previously logged in
        _connection.changeUser(token);
      }
      setState(() {
        _loading = true;
      });
      saveToken(token);
      Api a = Api(token);
      try {
        User self = await a.getSelf();
        setState(() {
          _loading = false;
          _api = a;
          _self = self;
          _hasLoggedIn = true;
        });
      } catch (e) {
        print(e);
        //Token is not valid or no internet connection
        setState(() {
          _loading = false;
          _hasLoggedIn = false;
        });
      }
    }
  }

  void logOut() async {
    await removeToken();
    _connection.logOut();
    removeGame();
    setState(() {
      _hasLoggedIn = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget view = Center(
      child: CircularProgressIndicator(),
    );

    if (!_loading && !_hasLoggedIn) {
      view = Login(
        didLogin: didLogin,
      );
    } else if (!_loading && _hasLoggedIn) {
      if (_self.isAdmin) {
        view = AppState(
            self: _self, logOut: logOut, api: _api, child: AdminView());
      } else {
        view = AppState(
          child: GameWrapper(),
          self: _self,
          api: _api,
          logOut: logOut,
          connection: _connection,
        );
      }
    }

    return Scaffold(
      backgroundColor: Color.fromRGBO(28, 58, 92, 1),
      body: AnimatedSwitcher(
        duration: Duration(milliseconds: 200),
        child: view,
      ),
    );
  }
}
