import 'dart:async';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import '../models/User.dart';

class GameConnection {
  Function _onConnect;
  Function _onDisconnect;
  Function _onJoin;

  StreamController<int> prize = StreamController<int>.broadcast();
  StreamController<int> clicksToNextPrize = StreamController<int>.broadcast();
  StreamController<int> tokens = StreamController<int>.broadcast();
  StreamController<List<User>> scoreboard =
      StreamController<List<User>>.broadcast();

  IO.Socket _socket;
  bool _disposed = false;
  bool isConnected = false;
  String _game;

  void dispose() {
    _disposed = true;
    clicksToNextPrize?.close();
    tokens?.close();
    prize?.close();
    scoreboard?.close();
    _socket?.disconnect();
    _socket.close();
    _socket.destroy();
    _onConnect = null;
    _onDisconnect = null;
  }

  set onConnect(onConnect) => _onConnect = onConnect;
  set onDisconnect(onDisconnect) => _onDisconnect = onDisconnect;
  set onJoin(onJoin) => _onJoin = onJoin;

  void click() {
    _socket.emit("click");
  }

  void logOut() {
    clearSocketListeners();
    _socket.emit("logOut");
  }

  void resetTokens() {
    _socket.emit("resetTokens");
  }

  void clearSocketListeners() {
    _socket.on("nextPrize:$_game", (_) {});
    _socket.on("users:$_game", (_) {});
    _game = null;
  }

  void leaveGame() {
    _socket.emit("leaveGame");
    clearSocketListeners();
  }

  void changeUser(String token) {
    _socket.emit("changeUser", {"token": token});
  }

  void joinGame(String name) {
    assert(_onJoin != null);

    if (_game != null) clearSocketListeners();

    _game = name;

    //Connected event is triggered when game join is completed successfully
    _socket.on("connected", (_) {
      _onJoin();
    });

    //NextPrize is triggered after a game user clicks the button. nextPrice returns the clicks until next prize
    _socket.on("nextPrize:$name", (dynamic clicksToNext) {
      clicksToNextPrize.sink.add(clicksToNext);
    });

    //Triggered after click. Returns the prize of the click
    _socket.on('clickResponse', (dynamic _prize) {
      prize.sink.add(_prize);
    });

    _socket.on("tokens", (dynamic _tokens) {
      tokens.add(_tokens);
    });

    //Returns the current users
    _socket.on("users:$name", (dynamic _users) {
      List<Map> users = List<Map>.from(_users);
      scoreboard.sink
          .add(<User>[...users.map((m) => User.fromJson(m)).toList()]);
    });
    _socket.emit("joinGame", {"game": name});
  }

  Future<void> connect(String url, String token) async {
    assert(_onConnect != null && _onDisconnect != null);
    isConnected = true;
    print("Connecting");
    Map options = {
      'transports': ['websocket'],
      "query": {
        "token": token,
      }
    };

    _socket = IO.io(url, options);
    _socket.on('connect', _onConnect);

    _socket.on("error", (e) {
      print(e);
    });

    _socket.on('disconnect', (_) {
      //If we dispose the connection disconnect event gets triggered. We don't want that reconnecting dialog
      if (!_disposed) _onDisconnect();
    });
  }
}
