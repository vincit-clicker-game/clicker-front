import 'package:flutter/services.dart';

class ReplaceEmptyInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
        String text = newValue.text;

    if (text.isEmpty) {
      return TextEditingValue(text: "0");
    }
    if(text.startsWith("0")&&text.length>1){
      return TextEditingValue(text: text.substring(1));
    }

    return newValue;
  }
}
