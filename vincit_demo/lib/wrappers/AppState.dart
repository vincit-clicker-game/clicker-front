import 'package:flutter/material.dart';
import 'package:vincit_demo/stream/GameConnection.dart';
import 'package:vincit_demo/models/User.dart';
import 'package:vincit_demo/providers/api.dart';

//Provide handy variables down the render tree

class AppState extends InheritedWidget {
  const AppState({
    Key key,
    this.connection,
    this.self,
    this.api,
    this.logOut,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);
  final Function logOut;
  final User self;
  final Api api;
  final GameConnection connection;
  static AppState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppState>();
  }

  @override
  bool updateShouldNotify(AppState old) => true;
}
