import 'package:flutter/material.dart';

class GameAppBar extends StatelessWidget implements PreferredSizeWidget {

  const GameAppBar({Key key, this.showLeading, this.onClose, this.title, this.logOut}) : super(key: key);
  final bool showLeading;
  final Function onClose;
  final Widget title;
  final Function logOut;

  @override
  Widget build(BuildContext context) {
    return AppBar(
        title:title,
        leading: showLeading
            ? IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  onClose();
                },
              )
            : null,
        backgroundColor: Color(0xff1c3a5c),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              logOut();
            },
          )
        ],
      );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}