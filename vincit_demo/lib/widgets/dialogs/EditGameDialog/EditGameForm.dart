import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/models/Prize.dart';

import 'EditPrizes.dart';

typedef OnGameFormSubmit(Game game);

class EditGameForm extends StatefulWidget {
  EditGameForm({Key key, this.game, this.onSubmit, this.title})
      : super(key: key);
  final Game game;
  final OnGameFormSubmit onSubmit;
  final String title;

  @override
  _EditGameFormState createState() => _EditGameFormState();
}

class _EditGameFormState extends State<EditGameForm> {
  final _formKey = GlobalKey<FormState>();
  Game _newGame;

  @override
  void initState() {
    super.initState();
    _newGame = widget.game;
  }

  void delete(Prize prize) {
    setState(() {
      _newGame.prizes.remove(prize);
    });
  }

  String nameValidator(String value) {
    return value.isNotEmpty ? null : "Value is too short!";
  }

  double getWidth() {
    Size size = MediaQuery.of(context).size;
    if (size.height > size.width) {
      return size.width * 0.8;
    }
    return size.width * 0.3;
  }

  void onSavePrize(Prize prize) {
    _newGame.prizes.add(prize);
  }

  String validatePrizes(List<Prize> values) {
    values.forEach((Prize element) { 

      print(element.toJson());
    });
    return values.every(
            (Prize current) => (current.prize!=0) && (current.nth!=0))
        ? null
        : "Please input non-zero value";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 500,
      constraints: BoxConstraints(maxWidth: getWidth()),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                widget.title,
                style: Theme.of(context).textTheme.headline5,
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  initialValue: widget.game.name,
                  onSaved: (String value) {
                    _newGame.name = value;
                  },
                  validator: nameValidator,
                  decoration: InputDecoration(labelText: "Name"),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  initialValue: widget.game.imgUrl,
                  onSaved: (String value) {
                    _newGame.imgUrl = value;
                  },
                  decoration: InputDecoration(labelText: "Image url"),
                ),
              ),
              Text(
                "Prizes",
                style: Theme.of(context).textTheme.headline6,
              ),
              EditPrizesField(
                initialValue: _newGame.prizes,
                validator: validatePrizes,
              ),
              ButtonBar(
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Cancel"),
                  ),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        _newGame.prizes.add(Prize(null, null));
                      });
                    },
                    child: Text("Add prize"),
                  ),
                  FlatButton(
                    child: Text("Submit"),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        print("pass");
                        _formKey.currentState.save();
                        widget.onSubmit(_newGame);
                      }
                    },
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
