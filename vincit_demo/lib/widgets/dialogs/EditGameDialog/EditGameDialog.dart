import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/widgets/dialogs/EditGameDialog/EditGameForm.dart';


void showEditDialog(Game game, BuildContext context, Function submit, String title){
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return SimpleDialog(
        children: <Widget>[
          EditGameForm(
            onSubmit: submit,
            title: title,
            game: game,
          ),
        ]
      );});
}