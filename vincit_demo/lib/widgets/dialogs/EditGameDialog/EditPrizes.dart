import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vincit_demo/inputFormatters/ReplaceEmptyInputFormatter.dart';
import 'package:vincit_demo/models/Prize.dart';

//Had to try extension methods
extension safeConversion on int {
  String toSafeString() {
    return this == null ? "" : this.toString();
  }
}

class EditPrizesField extends FormField<List<Prize>> {
  static void delete(Prize prize, FormFieldState<List<Prize>> state) {
    List<Prize> currentState = state.value;
    currentState.remove(prize);
    state.didChange(currentState);
  }

  static double getMaxHeight(BuildContext context) {
    return MediaQuery.of(context).size.height * 0.8;
  }

  static String getError(String text, FormFieldState<List<Prize>> state) {
    if (!state.hasError) return null;
    return text == null || text=="0" ? state.errorText : null;
  }

  EditPrizesField(
      {FormFieldSetter<List<Prize>> onSaved,
      FormFieldValidator<List<Prize>> validator,
      List<Prize> initialValue,
      bool autovalidate = false})
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<List<Prize>> state) {
              List<Prize> currentState = state.value;
              print(state.errorText);
              return Container(
                constraints:
                    BoxConstraints(maxHeight: getMaxHeight(state.context)),
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: currentState.length,
                    itemBuilder: (BuildContext context, int index) {
                      Prize prize = currentState[index];
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Flexible(
                              child: TextFormField(
                                inputFormatters: <TextInputFormatter>[
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  ReplaceEmptyInputFormatter()
                                ],
                                keyboardType: TextInputType.number,
                                onChanged: (String value) {
                                  currentState[index].nth = int.parse(value);
                                  state.didChange(currentState);
                                },
                                decoration: InputDecoration(
                                    labelText: "Every nth",
                                    errorText: getError(
                                        prize.nth.toSafeString(), state)),
                                maxLength: 3,
                                initialValue: prize.nth.toSafeString(),
                              ),
                            ),
                            Flexible(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: TextFormField(
                                  inputFormatters: <TextInputFormatter>[
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    ReplaceEmptyInputFormatter()
                                  ],
                                  keyboardType: TextInputType.number,
                                  onChanged: (String value) {
                                    value = value.isEmpty? "0" : value;
                                    currentState[index].prize =
                                        int.parse(value);
                                    state.didChange(currentState);
                                  },
                                  decoration: InputDecoration(
                                      labelText: "Prize",
                                      errorText: getError(
                                          prize.prize.toSafeString(), state)),
                                  maxLength: 3,
                                  initialValue: prize.prize.toSafeString(),
                                ),
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                delete(prize, state);
                              },
                              icon: Icon(
                                Icons.close,
                                color: Colors.red,
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
              );
            });
}
