import 'package:flutter/material.dart';

void showLoadingDialog(String text, context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return SimpleDialog(title: Center(child:Text(text)), children: <Widget>[
        Center(child: CircularProgressIndicator()),
      ]);
    },
  );
}
