import 'package:flutter/material.dart';

void showDisconnectDialog(BuildContext context) {
  showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return SimpleDialog(
        title: Text('Disconnected'),
      );
    },
  );
}
