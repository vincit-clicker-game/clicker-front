import 'package:flutter/material.dart';

void showOutOfTokensDialog(BuildContext context, Function callback, String text){
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(title: Center(child:Text(text)), actions: <Widget>[
        FlatButton(child: Text("I like being tokenless"), onPressed: (){
          Navigator.pop(context);
          },),
        FlatButton(child: Text("Reset tokens"), onPressed: callback,)
      ],);
    },
  );
}