import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Prize.dart';

String getEnding(int num) {
  switch (num % 10) {
    case 1:
      return "st";
    case 2:
      return "nd";
    case 3:
      return "rd";
    default:
      return "th";
  }
}

void showPrizeDialog(List<Prize> prizes, BuildContext context) {
  //Looks awful I know
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Prizes"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List<Widget>.from(prizes.map((Prize prize) {
              return Text(
                  'Every ${prize.nth}${getEnding(prize.nth)}, prize: ${prize.prize}');
            }).toList()),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: Navigator.of(context).pop,
              child: Text("Ok"),
            )
          ],
        );
      });
}
