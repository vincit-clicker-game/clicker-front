import 'package:flutter/material.dart';
import 'package:vincit_demo/widgets/login/LoginTextField.dart';

import 'createUserButton.dart';

class CreateUserForm extends StatefulWidget {
  const CreateUserForm(
      {Key key, this.onSubmit, this.loading, this.error, this.switchMode})
      : super(key: key);
  final Function onSubmit;
  final Function switchMode;
  final bool loading;
  final String error;
  @override
  _CreateUserFormState createState() => _CreateUserFormState();
}

class _CreateUserFormState extends State<CreateUserForm> {
  final _formKey = GlobalKey<FormState>();

  //Bug: error is not cleared when starting typing

  String username = "";
  String pass1 = "";
  String pass2 = "";

  String passDontMatch;

  void submit() {
    if (widget.loading) {
      return;
    }
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      if (pass1 != pass2) {
        setState(() {
          passDontMatch = "Passwords don't match";
        });
        return;
      }
      widget.onSubmit(username, pass1);
    }
  }

  String validateUsername(String val) {
    if (val.isEmpty || val.length > 16) {
      return "Username if of incorrect length";
    }
    return null;
  }

  String validatePassword(String val) {
    if (val.isEmpty) {
      return "Insert password";
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    String initialUsername = "";
    String initialPass = "";

    assert(() {
      initialUsername = "peura";
      initialPass = "porojenarmada";
      return true;
    }());

    //Absolutely crappy ui code
    return Form(
      key: _formKey,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Text(
            "Vincit demo",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 40,
              color: Colors.white,
            ),
          ),
          Text(
            "Please create an account",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 0.8, top: 18, bottom: 8),
              child: LoginTextField(
                initialText: initialUsername,
                obscureText: false,
                inputType: TextInputType.emailAddress,
                errorText: widget.error,
                helperText: "Username",
                validator: validateUsername,
                onSaved: (text) {
                  username = text;
                },
              )),
          Container(
            height: 5,
          ),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: LoginTextField(
                initialText: initialPass,
                obscureText: true,
                inputType: TextInputType.text,
                errorText: passDontMatch,
                helperText: "Password",
                validator: validatePassword,
                onSaved: (text) {
                  pass1 = text;
                },
              )),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: LoginTextField(
                initialText: initialPass,
                obscureText: true,
                inputType: TextInputType.text,
                errorText: passDontMatch,
                helperText: "Password",
                validator: validatePassword,
                onSaved: (text) {
                  pass2 = text;
                },
              )),
          CreateUserButton(
            submit: submit,
            loading: widget.loading,
          ),
          if (!widget.loading)
            FlatButton(
              onPressed: () {
                //Return to login view
                widget.switchMode();
              },
              child: Text(
                "Wait, no, I did have an account already",
                style: TextStyle(color: Colors.white),
              ),
            )
        ],
      ),
    );
  }
}
