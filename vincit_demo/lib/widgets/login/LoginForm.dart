import 'package:flutter/material.dart';

import 'LoginTextField.dart';

class LoginForm extends StatefulWidget {
  const LoginForm(
      {Key key, this.onSubmit, this.loading, this.error, this.switchMode})
      : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
  final Function onSubmit;
  final Function switchMode;
  final bool loading;
  final String error;
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  //Bug: error is not cleared when starting typing

  String username = "";
  String pass = "";

  String validateUsername(String val) {
    if (val.isEmpty|| val.length > 16) {
      return "Username if of incorrect length";
    }
    return null;
  }

  String validatePassword(String val) {
    if (val.isEmpty) {
      return "Insert password";
    }
    return null;
  }

  void submit() {
    if (!widget.loading && _formKey.currentState.validate()) {
      _formKey.currentState.save();
      widget.onSubmit(username, pass);
    }
  }

  @override
  Widget build(BuildContext context) {
    String initialUsername = "";
    String initialPass = "";

    //Debug credentials
    assert(() {
      initialUsername = "peura";
      initialPass = "porojenarmada";
      return true;
    }());

    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Vincit demo",
            style: TextStyle(fontSize: 40, color: Colors.white),
          ),
          Text(
            "Please log in",
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          Container(
            height: 10,
          ),
          Padding(
              padding: const EdgeInsets.only(
                  top: 8.0, left: 8, right: 8, bottom: 13),
              child: LoginTextField(
                initialText: initialUsername,
                obscureText: false,
                inputType: TextInputType.emailAddress,
                errorText: widget.error,
                helperText: "Username",
                validator: validateUsername,
                onSaved: (text) {
                  username = text;
                },
              )),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: LoginTextField(
                initialText: initialPass,
                obscureText: true,
                inputType: TextInputType.text,
                errorText: widget.error,
                helperText: "Password",
                validator: validatePassword,
                onSaved: (text) {
                  pass = text;
                },
              )),
          Row(
            children: <Widget>[
              Expanded(
                child: OutlineButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0)),
                  color: Colors.white,
                  child: widget.loading
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.white),
                          ),
                        )
                      : Text(
                          "Log in",
                          style: TextStyle(color: Colors.white),
                        ),
                  onPressed: submit,
                ),
              ),
            ],
          ),
          if (!widget.loading)
            FlatButton(
              onPressed: () {
                widget.switchMode();
              },
              child: Text(
                "Create account",
                style: TextStyle(color: Colors.white),
              ),
            )
        ],
      ),
    );
  }
}
