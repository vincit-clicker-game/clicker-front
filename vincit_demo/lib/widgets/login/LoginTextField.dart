import 'package:flutter/material.dart';

class LoginTextField extends StatelessWidget {
  final String initialText;
  final TextInputType inputType;
  final String errorText;
  final String helperText;
  final Function validator;
  final Function onSaved;
  final bool obscureText;

  const LoginTextField({
    Key key,
    this.initialText,
    this.inputType,
    this.errorText,
    this.helperText,
    this.validator,
    this.onSaved,
    this.obscureText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initialText,
      obscureText: obscureText,
      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
      keyboardType: inputType,
      decoration: InputDecoration(
        errorText: errorText,
        helperStyle:
            TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
        fillColor: Colors.white,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 0.5),
        ),
        helperText: helperText,
        border: OutlineInputBorder(),
      ),
      validator: validator,
      onSaved: onSaved,
    );
  }
}
