import 'package:flutter/material.dart';

class CreateUserButton extends StatelessWidget {
  final Function submit;
  final bool loading;

  const CreateUserButton({Key key, this.submit, this.loading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: OutlineButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4.0)),
            color: Colors.white,
            child: loading
                ? Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            )
                : Text(
              "Create account",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: submit,
          ),
        ),
      ],
    );
  }
}
