import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/widgets/game/ImgPlaceholder.dart';
import 'package:vincit_demo/widgets/dialogs/PrizeDialog.dart';

typedef void GameCardClick(Game game);

class GameCard extends StatelessWidget {

  final Game game;
  final GameCardClick onClick;
  final String action;
  const GameCard({Key key, this.game, this.onClick, this.action}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 300),
      child: AspectRatio(
        aspectRatio: 1,
        child: InkWell(
          onTap: () {},
          child: Card(
            child: Stack(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 200,
                      child: Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 10.0,
                              spreadRadius: 1.0,
                              offset: Offset(
                                10.0,
                                10.0,
                              ),
                            )
                          ],
                        ),
                        child: Stack(
                          children: <Widget>[
                            ImgElement(
                              url: game.imgUrl,
                              placeholderColor: Color(0xff1c3a5c),
                            ),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  game.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Creator: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(game.creator)
                        ],
                      ),
                    )
                  ],
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: MaterialButton(
                          onPressed: () {
                            showPrizeDialog(game.prizes, context);
                          },
                          child: Text(
                            "View prizes",
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ),
                      Expanded(
                        child: MaterialButton(
                          onPressed: () {
                            onClick(game);
                          },
                          child: Text(
                            action==null?"Join game":action,
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
