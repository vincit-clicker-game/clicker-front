import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

//This component shows an image when available, otherwise solid color
//Photos are not currently in use

class ImgElement extends StatelessWidget {
  final String url;
  final Color placeholderColor;
  const ImgElement({Key key, this.url, this.placeholderColor})
      : super(key: key);

  bool isValidUrl() {
    return url != null && url.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return isValidUrl()
        ? CachedNetworkImage(
            imageUrl: url,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
          )
        : Container(
            color: placeholderColor,
          );
  }
}
