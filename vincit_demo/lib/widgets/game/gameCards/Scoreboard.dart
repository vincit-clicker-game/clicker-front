import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vincit_demo/models/User.dart';
import 'package:vincit_demo/widgets/dialogs/PrizeDialog.dart';

import 'GameCard.dart';

class ScoreBoard extends StatelessWidget {
  final StreamController controller;

  const ScoreBoard({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GameCard(
      titleString: "Scoreboard",
      child: StreamBuilder<Object>(
          stream: controller.stream,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            }

            List<User> users = List<User>.from(snapshot.data);
            users.sort((User a, User b) {
              return b.tokens - a.tokens;
            });

            return Table(
              border: TableBorder.all(),
              children: <TableRow>[
                TableRow(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Position"),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Username"),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Tokens"),
                    ),
                  ],
                ),
                ...List<TableRow>.from(
                    users.asMap().keys.toList().map((int index) {
                  User user = users[index];
                  return TableRow(children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child:
                          Text((index + 1).toString() + getEnding(index + 1)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(user.userName),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(user.tokens.toString()),
                    ),
                  ]);
                }))
              ],
            );
          }),
    );
  }
}
