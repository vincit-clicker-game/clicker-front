import 'package:flutter/material.dart';
import 'package:vincit_demo/stream/GameConnection.dart';
import 'package:vincit_demo/wrappers/AppState.dart';

import 'GameCard.dart';

class ClickTarget extends StatelessWidget {
  final GameConnection connection;

  const ClickTarget({
    Key key,
    this.connection,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GameCard(
      titleWidget: StreamBuilder(
        stream: connection.clicksToNextPrize.stream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return Text("");
          }
          return Text("Next prize in ${snapshot.data}");
        },
      ),
      child: StreamBuilder<int>(
          stream: connection.tokens.stream,
          builder: (context, snapshot) {
            int _tokens;
            if (snapshot.hasData) {
              _tokens = snapshot.data;
            } else {
              _tokens = AppState.of(context).self.tokens;
            }

            return MaterialButton(
              height: 300,
              minWidth: 300,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(200.0),
                side: BorderSide(color: Theme.of(context).accentColor),
              ),
              child: Text(
                "Click!",
                style: TextStyle(color: Colors.white, fontSize: 40),
              ),
              color: Theme.of(context).accentColor,
              onPressed: _tokens == 0
                  ? null
                  : () {
                      connection.click();
                    },
            );
          }),
    );
  }
}
