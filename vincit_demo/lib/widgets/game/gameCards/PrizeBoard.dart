import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/models/PrizeCount.dart';

import 'GameCard.dart';

class PrizeBoard extends StatefulWidget {
  final Game game;
  final StreamController<int> prizes;

  const PrizeBoard({Key key, this.game, this.prizes}) : super(key: key);

  @override
  _PrizeBoardState createState() => _PrizeBoardState();
}

class _PrizeBoardState extends State<PrizeBoard> {
  List<PrizeCount> victories = [];
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    assert(widget.game != null && widget.prizes != null,
        "Game and prizes can't be null");
    victories = toPrizeCount(widget.game.prizes);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _subscription = widget.prizes.stream.listen((int data) {
      if (data == 0) return;
      PrizeCount hit = victories.firstWhere((PrizeCount count) {
        return count.prize == data;
      });
      hit.amount++;
      setState(() {
        victories[victories.indexOf(hit)] = hit;
      });
    });
  }

  @override
  void dispose() {
    _subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GameCard(
      titleString: "Prizeboard",
      child: Table(
        border: TableBorder.all(),
        children: <TableRow>[
          TableRow(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Prize"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Every"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Times won"),
              ),
            ],
          ),
          ...List<TableRow>.from(victories.map((PrizeCount count) {
            return TableRow(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  count.prize.toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  count.nth.toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  count.amount.toString(),
                ),
              ),
            ]);
          }))
        ],
      ),
    );
  }
}
