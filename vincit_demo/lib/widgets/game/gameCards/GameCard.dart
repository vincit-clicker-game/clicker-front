import 'package:flutter/material.dart';

class GameCard extends StatelessWidget {
  final Widget titleWidget;
  final Widget child;
  final String titleString;

  const GameCard(
      {Key key, this.titleString, @required this.child, this.titleWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    assert(
        (titleString == null && titleWidget != null) ||
            (titleString != null && titleWidget == null),
        "Define only titleWidget or titleString not both");

    Widget titleText;
    if (titleString != null) {
      titleText = Text(titleString);
    } else {
      titleText = titleWidget;
    }

    return Card(
      child: Container(
        height: 400,
        width: 400,
        child: Column(
          children: <Widget>[
            AppBar(
              backgroundColor: Color.fromRGBO(28, 58, 92, 1),
              title: titleText,
            ),
            Padding(padding: const EdgeInsets.all(8.0), child: child),
          ],
        ),
      ),
    );
  }
}
