import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';

import 'GameCard.dart';

class GameRow extends StatelessWidget {

  final List<Game> games;
  final GameCardClick onClick;
  final String action;
  const GameRow({Key key, this.games, this.onClick, @required this.action, }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    if (games == null || games.isEmpty) {
      return Text(
        "No games found",
        style:
            Theme.of(context).textTheme.headline6.copyWith(color: Colors.white),
      );
    }

    return Wrap(
      children: games
          .map((Game game) => GameCard(
              action: action,
                game: game,
                onClick: onClick,
              ))
          .toList(),
    );
  }
}
