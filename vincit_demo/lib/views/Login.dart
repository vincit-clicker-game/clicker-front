import 'package:flutter/material.dart';
import 'package:vincit_demo/providers/api.dart';
import 'package:vincit_demo/widgets/login/CreateUserForm.dart';
import 'package:vincit_demo/widgets/login/LoginForm.dart';

class Login extends StatefulWidget {
  const Login({Key key, this.didLogin}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
  final Function didLogin;
}

class _LoginState extends State<Login> {
  //Whether we are in a login or createAccountView
  bool showLoginView = true;

  String username = "";
  String pass = "";

  bool loading = false;
  String error;

  switchMode() {
    //Switch between login view and create user view
    setState(() {
      showLoginView = !showLoginView;
      error = null;
    });
  }

  onLogin(String username, String password) async {
    //Do a login attempt
    setState(() {
      loading = true;
      error = null;
    });

    Api.login(username, password).catchError((Object _e) {
      Exception exception = _e;
      if(_e.toString() == "XMLHttpRequest error." ){
      setState(() {
        error = "Network error";
        loading = false;
      });
      }else{
        setState(() {
        error = exception.toString();
        loading = false;
      });
      }
      
    }).then((String token) {
      setState(() {
        loading = false;
      });
      widget.didLogin(token);
    });
  }

  onCreateAccount(String username, String password) {
    //Attempt to create an account
    setState(() {
      loading = true;
      error = null;
    });

    //CreateUser only returns a boolean, not token of any kind
    Api.createUser(username, password).then((_) {
      setState(() {
        loading = false;
        error = null;
      });
      switchMode();
    }).catchError((e) {
      Exception exception = e;
      setState(() {
        error = exception.toString();
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          constraints: BoxConstraints(maxWidth: 600),
          child: AnimatedSwitcher(
            duration: Duration(milliseconds: 200),
            child: showLoginView
                ? LoginForm(
                    error: error,
                    loading: loading,
                    onSubmit: onLogin,
                    switchMode: switchMode,
                  )
                : CreateUserForm(
                    error: error,
                    loading: loading,
                    onSubmit: onCreateAccount,
                    switchMode: switchMode,
                  ),
          )),
    );
  }
}
