import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/providers/storage.dart';
import 'package:vincit_demo/widgets/misc/AppBar.dart';
import 'package:vincit_demo/widgets/game/GameRow.dart';
import 'package:vincit_demo/widgets/dialogs/OutOfTokensView.dart';
import 'package:vincit_demo/wrappers/AppState.dart';
import 'package:vincit_demo/models/Game.dart' as GameModel;

import 'GameView.dart';

class GameWrapper extends StatefulWidget {
  @override
  _GameWrapperState createState() => _GameWrapperState();
}

class _GameWrapperState extends State<GameWrapper> {
  List<GameModel.Game> _games;
  bool _loading = true;
  bool _inGame = false;
  Game _game;
  int _tokens = 0;
  StreamSubscription _subscription;

  @override
  void dispose() {
    _subscription?.cancel();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _tokens = AppState.of(context).self.tokens;

    if (_tokens == 0) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (_inGame) {
          onClose();
        }
        showOutOfTokensDialog(context, () {
          AppState.of(context).connection.resetTokens();
          Navigator.of(context).pop();
        }, "Out of tokens. Please refill");
      });
    }

    _subscription =
        AppState.of(context).connection.tokens.stream.listen((int tokens) {
      print("Received token stream");
      setState(() {
        _tokens = tokens;
      });
      if (tokens == 0) {
        showOutOfTokensDialog(context, () {
          AppState.of(context).connection.resetTokens();
          Navigator.of(context).pop();
        }, "Out of tokens. Please refill");
        if (_inGame) {
          onClose();
        }
      }
    });

    //Load games from server
    AppState.of(context).api.getGames().catchError((e) {
      setState(() {
        _loading = false;
        _games = [];
      });
    }).then((List<GameModel.Game> games) async {
      //Load game name from local storage
      String gameName = await getGame();
      try {
        Game game = games.firstWhere(
          (Game curr) {
            return curr.name == gameName;
          },
        );
        if (game == null) {
          //No match
          setState(() {
            _loading = false;
            _games = games;
          });
          return;
        }
        //Match found
        setState(() {
          _loading = false;
          _games = games;
          _inGame = true;
          _game = game;
        });
      } catch (_) {
        setState(() {
          _loading = false;
          _games = games;
        });
      }
    });
  }

  void onSelectGame(Game game) {
    if (_tokens == 0) {
      showOutOfTokensDialog(context, () {
        AppState.of(context).connection.resetTokens();
        Navigator.of(context).pop();
      }, "Joining a game requires tokens. Please refill");
      return;
    }
    saveGame(game);
    setState(() {
      _inGame = true;
      _game = game;
    });
  }

  void onClose() {
    setState(() {
      _inGame = false;
    });
    //Remove game from local storage
    removeGame();
    //Remove game from server
    AppState.of(context).connection.leaveGame();
  }

  @override
  Widget build(BuildContext context) {
    if (_loading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.white),
        ),
      );
    }

    AppState appState = AppState.of(context);

    return Scaffold(
      appBar: GameAppBar(
        title: StreamBuilder<Object>(
            stream: appState.connection.tokens.stream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text('Tokens: ${snapshot.data}');
              }
              return Text('Tokens: $_tokens');
            }),
        showLeading: _inGame,
        onClose: onClose,
        logOut: appState.logOut,
      ),
      body: Center(
          child: _inGame
              ? GameView(
                  game: _game,
                )
              : GameRow(
                action: "Join game",
                  onClick: onSelectGame,
                  games: _games,
                )),
    );
  }
}
