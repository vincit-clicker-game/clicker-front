import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/widgets/game/gameCards/ClickTarget.dart';
import 'package:vincit_demo/widgets/game/gameCards/PrizeBoard.dart';
import 'package:vincit_demo/widgets/game/gameCards/Scoreboard.dart';
import 'package:vincit_demo/wrappers/AppState.dart';

class GameView extends StatefulWidget {
  const GameView({
    Key key,
    this.game,
  }) : super(key: key);

  @override
  _GameViewState createState() => _GameViewState();
  final Game game;
}

class _GameViewState extends State<GameView> {
  bool loading = true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    AppState.of(context).connection.onJoin = () {
      setState(() {
        loading = false;
      });
    };
    AppState.of(context).connection.joinGame(widget.game.name);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Wrap(
      spacing: 50,
      runSpacing: 50,
      alignment: WrapAlignment.spaceAround,
      children: <Widget>[
        ScoreBoard(
          controller: AppState.of(context).connection.scoreboard,
        ),
        ClickTarget(
          connection: AppState.of(context).connection,
        ),
        PrizeBoard(
          game: widget.game,
          prizes: AppState.of(context).connection.prize,
        )
      ],
    ));
  }
}
