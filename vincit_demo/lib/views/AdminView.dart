import 'package:flutter/material.dart';
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/widgets/dialogs/EditGameDialog/EditGameDialog.dart';
import 'package:vincit_demo/widgets/misc/AppBar.dart';
import 'package:vincit_demo/widgets/game/GameRow.dart';
import 'package:vincit_demo/wrappers/AppState.dart';

class AdminView extends StatefulWidget {
  const AdminView({Key key}) : super(key: key);

  @override
  _AdminViewState createState() => _AdminViewState();
}

class _AdminViewState extends State<AdminView> {
  bool _loading = true;
  List<Game> _games;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    AppState.of(context).api.getGames().then((List<Game> games) {
      setState(() {
        _games = games;
        _loading = false;
      });
    });
  }

  onSubmitGameSelect(Game newGame){

  }

  onSelectGame(Game game) {
    showEditDialog(game, context, onSubmitGameSelect, "Edit game");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: GameAppBar(
          title: Text(
            "Admin view",
          ),
          showLeading: false,
          logOut: AppState.of(context).logOut,
        ),
        body: Center(
            child: _loading
                ? CircularProgressIndicator()
                : GameRow(
                  action: "Edit game",
                    games: _games,
                    onClick: onSelectGame,
                  )));
  }
}
