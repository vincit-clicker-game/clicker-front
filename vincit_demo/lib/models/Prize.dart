class Prize {
  Prize(this.nth, this.prize);

  int nth;
  int prize;

  //No need for getters or setters here as prize is used to save dynamic data  

  factory Prize.fromJson(Map data) {
    return Prize(data['nth'], data['prize']);
  }

  Map toJson() {
    return {"nth": nth, "prize": prize};
  }
}
