import 'package:vincit_demo/models/Prize.dart';

class Game {

  Game(this.name, this._counter, this.prizes, this.imgUrl, this._creator);

  String _creator;
  String name;
  int _counter;
  List<Prize> prizes;
  String imgUrl;

  get counter => _counter;
  get creator => _creator == null ? "Admin" : _creator;

  set counter(int counter) => counter = _counter;

  factory Game.fromJson(Map data) {
    
    if (data == null) return null;

    return Game(
        data['name'],
        data['counter'],
        List<Prize>.from(data['prizes'].map((m) => Prize.fromJson(m)).toList()),
        data['url'],
        data["creator"]);
  }

  Map<String, Object> toJson() {
    return Map<String, Object>.from({
      "name": name,
      "counter": _counter,
      "prizes": prizes.map((Prize m) => m.toJson()),
      "url": imgUrl,
      "creator": _creator
    });
  }
}
