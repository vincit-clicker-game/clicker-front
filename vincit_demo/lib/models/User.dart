class User {
  User(this._userName, this._tokens, this._admin);

  final String _userName;
  int _tokens;
  final bool _admin;

  set tokens(tokens) => tokens = _tokens;

  get tokens => _tokens;
  get userName => _userName;
  get isAdmin => _admin;

  factory User.fromJson(Map data) {
    print(data);
    return User(data['username'], data['coins'], data['admin']);
  }

  Map toJson() {
    return {"username": _userName, "coins": _tokens, "admin": _admin};
  }
}
