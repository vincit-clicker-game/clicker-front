import 'Prize.dart';

class PrizeCount extends Prize {
  PrizeCount(int nth, int prize) : super(nth, prize);
  factory PrizeCount.fromPrize(Prize prize) {
    return PrizeCount(prize.nth, prize.prize);
  }
  int amount = 0;
}

List<PrizeCount> toPrizeCount(List<Prize> prizes) {
  return prizes.map((Prize prize) {
    return PrizeCount.fromPrize(prize);
  }).toList();
}
