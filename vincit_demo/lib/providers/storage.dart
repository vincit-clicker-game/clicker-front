import 'package:crypted_preferences/crypted_preferences.dart';
import 'package:vincit_demo/models/Game.dart';

//Todo: migrate to class

Future<bool> isLoggedIn() async {
  final Preferences preferences =
      await Preferences.preferences(path: 'default');
  return preferences.getString('token', defaultValue: "").isNotEmpty;
}

void saveToken(String token) async {
  final Preferences preferences =
      await Preferences.preferences(path: 'default');
  preferences.setString("token", token);
}

Future<String> getToken() async {
  final Preferences preferences =
      await Preferences.preferences(path: 'default');
  return preferences.getString('token', defaultValue: "");
}

Future<String> getGame() async {
  final Preferences preferences =
      await Preferences.preferences(path: 'default');
  return preferences.getString("game");
}

saveGame(Game game) async {
  final Preferences preferences =
      await Preferences.preferences(path: 'default');
  preferences.setString("game", game.name);
}

removeGame() async {
  final Preferences preferences =
      await Preferences.preferences(path: 'default');
  preferences.setString("game", null);
}

Future<void> removeToken() async {
  final Preferences preferences =
      await Preferences.preferences(path: 'default');
  preferences.remove('token');
}
