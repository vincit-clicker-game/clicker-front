import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:vincit_demo/models/Game.dart';
import 'package:vincit_demo/models/User.dart';

String url = "http://" + (kIsWeb ? "localhost" : "10.0.2.2") + ":5001/";

class Api {
  String _token;

  Api(this._token);

  get token => _token;

  Future<List<Game>> getGames() async {
    try {
      http.Response  response = await makeRequest(
        url + "games",
        {},
        _token,
      );
      if (response.statusCode != 200) {
        print(response);
        throw Exception("Failed to retrieve the list of games");
      }
       return List<Game>.from(
        json.decode(response.body).map((m) => Game.fromJson(m)).toList());
    } catch (e) {
      print(e);
      throw Exception("Failed to retrieve the list of games");
    }
   
  }

  Future<User> getSelf() async {
    http.Response response = await makeRequest(url + "user", {}, _token);

    if (response.statusCode != 200) {
      throw Exception("Error fething user. Please try again later");
    }
    return User.fromJson(json.decode(response.body));
  }

  static Future<String> login(String username, String password) async {
    Map<String, String> body = {"username": username, "password": password};
    http.Response response = await makeRequest(url + "login", body);

    if (response.statusCode != 200) {
      print("Error: statuscode ${response.statusCode}");
      if (response.statusCode == 403) {
        throw Exception("Invalid username or password");
      } else {
        throw Exception("Error logging in. Please try again later");
      }
    }

    return json.decode(response.body)["token"];
  }

  static Future<http.Response> makeRequest(
      String url, Map<String, String> params,
      [String token]) async {
    Map<String, String> headers = {};
    if (token != null) {
      headers["authorization"] = "bearer " + token;
    }

    return await http.post(url, body: params, headers: headers);
  }

  static Future<void> createUser(String username, String password) async {
    Map<String, String> body = {"username": username, "password": password};
    http.Response response = await makeRequest(url + "createUser", body);

    //400 is the only known error status code
    if (response.statusCode == 400) {
      throw Exception("Username already exists");
    } else if (response.statusCode != 200) {
      throw Exception("Error creating user. Please try again later");
    }
  }
}
