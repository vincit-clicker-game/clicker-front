# Vincit demo front
This is only the front indented to be used as a submodule. Please see https://gitlab.com/vincit-clicker-game/clicker-server for the whole project.

## Requirements
Please use a unix-like system for your own sake (Tested on mac os and (arch) linux).
Should you use windows, good luck :)

### Mobile development
Flutter sdk

### Web development
* Flutter sdk beta
* (Google chrome)

## How to get started

### Mobile development
flutter run (-d followed by a device name if you have multiple devices installed)

### Web development
flutter run -d chrome (assuming you have google-chrome installed)
Running with chrome gives you a lot of nice tools like debugging and a hot restart.
If you are an anti-chrome-person
`flutter run -d web-server`
